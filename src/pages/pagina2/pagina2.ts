import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Pagina3Page } from '../index.pages'

/**
 * Generated class for the Pagina2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pagina2',
  templateUrl: 'pagina2.html',
})
export class Pagina2Page {

  mutantes:any[] = [
    {
      nombre : 'Magneto',
      poder : 'Controlar metales'
    },
    {
      nombre : 'Wolverine',
      poder : 'Regeneracion acelerada'
    },
    {
      nombre : 'Profesor X',
      poder : 'Poderes psiquicos'
    },
    {
      nombre : 'Tormenta',
      poder : 'Controlar el tiempo'
    }
  ]

  pag3:any = Pagina3Page;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  pagina3(mutante:any){
    this.navCtrl.push(Pagina3Page, { 'personaje' : mutante });
  }
}
