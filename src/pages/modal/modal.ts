import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  name:string = '';
  age:number = 0;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public view: ViewController) {
      this.name = this.navParams.get('nombre');
      this.age = this.navParams.get('edad');
  }

  close_params(){
    let data = { nombre : 'Andres', edad : 25};
    this.view.dismiss(data);
  }

  close(){
    this.view.dismiss();
  }

}
