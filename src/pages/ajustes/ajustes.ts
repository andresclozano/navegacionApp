import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ModalPage } from '../index.pages';

@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {
  }

  mostrar_modal(){
    let params = { nombre: 'Andres', edad: 24};
    let modal = this.modalCtrl.create(ModalPage, params);
        modal.present();
        modal.onDidDismiss((data)=>{
          console.log(`Salio ${data}`);
        })
  }

}
